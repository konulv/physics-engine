from .vector2D import Vector as Vect
from math import pi


class RigidBody:
    count = 0

    def __init__(self, xPos, yPos, xSize, ySize, xCOM, yCOM, drag=0.5, mass=1, xForce=0, yForce=0,
                 xVelocity=0, yVelocity=0, angularSpeed=0):

        self.number = self.count
        RigidBody.count += 1
        self.position = Vect(xPos, yPos)
        self.size = Vect(xSize, ySize)
        self.centerOfMass = Vect(xCOM, yCOM)
        self.dragCoefficient = drag
        self.mass = mass
        self.force = Vect(xForce, yForce)
        self.velocity = Vect(xVelocity, yVelocity)
        self.drag = Vect(0, 0)
        self.angularSpeed = angularSpeed
        self.angularDamp = 0
        self.torque = 0
        self.rotation = 0
        self.inertia = 0

    @property
    def netForce(self):
        # print(f'{self.force["x"]} + {self.drag["x"]} = {self.force["x"] + self.drag["x"]}')
        x = self.force.x + self.drag.x
        y = self.force.y + self.drag.y

        return Vect(x, y)

    @property
    def netTorque(self):
        return self.torque + self.angularDamp

    def applyForce(self, forceVect):
        self.force += forceVect

    def updatePos(self, xPos, yPos):
        move = Vect(xPos, yPos)
        self.position += move

        self.centerOfMass += move

    def updateRotation(self, degree):
        # print(self.rotation, degree)
        # print(self.angularSpeed)
        self.rotation += degree
        if self.rotation >= pi*2:
            self.rotation -= pi*2

    def updateVelocity(self, velocityVect, angularSpeed):
        self.angularSpeed = angularSpeed
        self.velocity = velocityVect
        self.updateDrag()

    def updateDrag(self):
        # simple drag function for now
        # drag goes the opposite way of velocity
        mag = self.dragCoefficient * self.velocity.mag **2
        drag = self.velocity * -1
        drag.mag = mag

        self.drag = drag

        damp = (self.dragCoefficient+1.5) * self.angularSpeed ** 2
        # damp *= -1
        self.angularDamp = -damp if self.angularSpeed > 0 else damp



