from .rigidBody import RigidBody


class Circle(RigidBody):

    def __init__(self, radius, xPos, yPos, drag=0.5, mass=1, xForce=0, yForce=0,
                 xVelocity=0, yVelocity=0, angularSpeed=0):
        xSize = radius*2
        ySize = xSize
        xCOM = xPos #+ radius
        yCOM = yPos #+ radius
        super().__init__(xPos, yPos, xSize, ySize, xCOM, yCOM, drag, mass, xForce, yForce, xVelocity, yVelocity, angularSpeed)
        self.radius = radius
        self.inertia = 1 * mass * radius**2

