class Vector:
    def __init__(self, x, y, magnitude=-1):
        if x != 0 or y != 0:
            self.__normalize__(x, y, magnitude)
        else:
            self.__x = x
            self.__y = y
            self.__magnitude = 0

    def cross(self, other):
        return self.x * other.y - self.y * other.x


    @property
    def x(self):
        return self.__x * self.__magnitude

    # @x.setter
    # def x(self, x):
    #
    #     self.__x = x

    @property
    def y(self):
        return self.__y * self.__magnitude

    # @y.setter
    # def y(self, y):
    #     self.__y = y

    @property
    def mag(self):
        return self.__magnitude

    @mag.setter
    def mag(self, mag):
        self.__magnitude = mag

    def __normalize__(self, x, y, mag=-1):
        if mag == -1:  # non normalized vector is provided
            self.__magnitude = (x ** 2 + y ** 2) ** 0.5
        else:  # normalizing vector
            self.__magnitude = magnitude

        self.__x = x / self.__magnitude
        self.__y = y / self.__magnitude

    def __str__(self):
        return f"({self.__x}, {self.__y}, {self.mag})"

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    def __sub__(self, other):
        if type(other) == type(self):
            x = self.x - other.x
            y = self.y - other.y
        else:
            raise TypeError("ya done goofed, idk what to say")

        return Vector(x, y)

    def __mul__(self, other): # a normal dot product
        if type(other) == type(self):  # vector * vector
            return self.x * other.x + self.y * other.y
        elif type(other) == float or type(other) == int:  # float or int
            return Vector(self.x * other, self.y * other)
        else:
            raise TypeError("ya done goofed, idk what to say")

    def __pow__(self, other):  # dot product between normalized vectors
        # print((self.__x * other.__x), (self.__y * other.__y))
        val = (self.__x * other.__x) + (self.__y * other.__y)
        # print(f"dot product: {val}")
        if val > 1:
            return 1
        elif val < -1:
            return -1
        else:
            return val
        # return self.__x * other.__x + self.__y * other.__y

    def __truediv__(self, other):
        return Vector(self.x/other, self.y/other)

    def cross(self, other, n):
        if type(other) == type(self): # cross with another vector
            return self.x * other.y - self.y * other.x
        else: #number been given
            n.mag = self.mag * other
            return n

    def inverse(self):
        return Vector(-self.y, self.x)


if __name__ == "__main__":
    import math
    a = Vector(5, 5)
    b = Vector(5, 1)
    c = Vector(12, 3)
    print(a)
    #print(a, b, c)
    #print(a+b)
    #print()
    #print(math.acos(a ** b))

