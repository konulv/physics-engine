import data
# from .models import *
# http://www.euclideanspace.com/physics/dynamics/collision/twod/index.htm
# https://books.google.co.uk/books?hl=en&lr=&id=d0NZDwAAQBAJ&oi=fnd&pg=PP1&ots=2KsACPgv2c&sig=yj4it3zokZU22l9RGKrPvOdZn4M&redir_esc=y#v=onepage&q&f=false physics book
# https://github.com/NVIDIAGameWorks/PhysX/tree/4.1/physx/source/foundation/src
# https://en.wikipedia.org/wiki/Elastic_collision
# https://books.google.co.uk/books?hl=en&lr=&id=4pnrHN8Y1AgC&oi=fnd&pg=PP1&dq=collision+theory&ots=NW2yXUGvNY&sig=Ziu0Ie5z76_0aeScMXKY2nLkjtw&redir_esc=y#v=onepage&q=collision%20theory&f=false
# https://books.google.co.uk/books?hl=en&lr=&id=uBZEU5BS5i4C&oi=fnd&pg=PA1&dq=collision+theory&ots=h_lYkQmK2v&sig=BIdaNH3a2TNFp-MxqpSWF4O1Gq8&redir_esc=y#v=onepage&q=collision%20theory&f=false
# https://www.researchgate.net/profile/Wolfgang_Christian/publication/265022969_An_Introduction_to_Computer_Simulation_Methods/links/5550920a08ae956a5d253c52/An-Introduction-to-Computer-Simulation-Methods.pdf

# https://en.wikipedia.org/wiki/Collision_response  on friction
# https://en.wikipedia.org/wiki/Friction
# https://en.wikipedia.org/wiki/Sliding_(motion)
# https://en.wikipedia.org/wiki/Rolling
# https://en.wikipedia.org/wiki/Collision
#
# looks useful:
# https://www.codeproject.com/Articles/1215961/Making-a-D-Physics-Engine-Mass-Inertia-and-Forces
# https://en.wikiversity.org/wiki/Physics_equations/Impulse,_momentum,_and_motion_about_a_fixed_axis
from models.vector2D import Vector as Vect



def doPhysics(objectsData, outsideVariables):
    # sort out the collisions
    e = outsideVariables["e"]
    for a in objectsData.getAllColliding():
        # print(i.number, [x.number for x in objectsData.getAllCollisionsOf(i)])
        for b in objectsData.getAllCollisionsOf(a):
            # print(objectsData.collision_train)

            # step 1: get contact point
            p = getContactPoint(a, b)
            # TODO this is only a patch solution, figure out how to deal with complex contact points
            if isinstance(p.x, complex) or isinstance(p.y, complex):
                continue  # contact point is complex, i.e collision ain't happening

            # step 2: working "r" - a point of contact in respects to object's CoM
            # get vector PO
            ap = a.centerOfMass - p
            ap.mag = a.radius

            bp = b.centerOfMass - p
            bp.mag = b.radius

            # step 3: follow the damn formula
            # get impulse J
            apu = a.velocity + ap.cross(a.angularSpeed, ap.inverse())  # A Point velocity
            bpu = b.velocity + bp.cross(b.angularSpeed, bp.inverse())  # B point velocity
            aJ = (bpu - apu) * ((a.mass*b.mass)/(a.mass + b.mass))*(e+1)  # TODO consider infinite mass
            bJ = Vect(-aJ.x, -aJ.y)
            # print(f"A: {a.centerOfMass}\nB: {b.centerOfMass}")
            # print(f"impulse of A onto B: {J}")

            # step 4: workout point and center of mass velocities
            # get point velocity
            apv = aJ/a.mass + a.velocity
            bpv = bJ/b.mass + b.velocity

            # get new angular velocity
            # derived from L = r.cross(p) and L = I * w
            aw = ap.cross(aJ, ap.inverse())/a.inertia
            bw = bp.cross(bJ, bp.inverse())/b.inertia

            # get linear velocity by
            # point velocity - perpendicular velocity = center of mass velosity
            # reverse of step 3
            acv = apv - ap.cross(aw, ap.inverse())
            bcv = bpv - bp.cross(bw, bp.inverse())


            # step 4: update

            # aV = getNewVelocity(a, b)
            # bV = getNewVelocity(b, a)
            #
            # print(iV, jV)
            # a.updateVelocity(aV)
            # b.updateVelocity(bV)

            a.updateVelocity(acv, aw)
            b.updateVelocity(bcv, bw)

            objectsData.removeCollision(a, b)


    # progress a tick
    tick = outsideVariables["tick"]

    for i in objectsData.getAllParticles():
        # v = u + at
        # s = (u + v) * t / 2
        xV = i.velocity.x + getAcceleration(i).x * tick
        # print(xV)
        yV = i.velocity.y + getAcceleration(i).y * tick
        xS = (i.velocity.x + xV) * tick / 2
        # print(xS)
        yS = (i.velocity.y + yV) * tick / 2

        angSpeed = i.angularSpeed + getAngularAcceleration(i) * tick

        degree = angSpeed*tick

        i.updateRotation(degree)
        i.updateVelocity(Vect(xV, yV), angSpeed)
        i.updatePos(xS, yS)
        # print(i.angularSpeed, i.netTorque)


def getContactPoint(a, b):
    ax = a.centerOfMass.x
    ay = a.centerOfMass.y
    ar = a.radius

    bx = b.centerOfMass.x
    by = b.centerOfMass.y
    br = b.radius


    # y = -x*c + k
    # c = (2*bx - 2*ax)/(2*by - 2*ay)
    # k = (bx**2 - ax**2 + by**2 - ay**2 + ar**2 - br**2)/(2*by - 2*ay)
    try:
        c = (2 * bx - 2 * ax) / (2 * by - 2 * ay)
    except ZeroDivisionError:  # (2by - 2ay) == 0, which means we have x on its own
        # this lets us work out x easily
        x = (bx**2 - ax**2 + ar**2 - br**2)/(2*bx - 2*ax)
        # given this x, we can now get both y's

        y1 = (x**2 - 2*x*ax + ax**2 + ar**2)**0.5
        y2 = -y1

        vect1 = Vect(x, y1)
        vect2 = Vect(x, y2)

        r = (vect1 + vect2) / 2
        return r

    k = (bx ** 2 - ax ** 2 + by ** 2 - ay ** 2 + ar ** 2 - br ** 2) / (2 * by - 2 * ay)

    # x**2 * g + x * h - i = 0
    # g = 1 + c**2
    # h = 2*(ay*c - ax - c*k)
    # i = ar**2 - ax**2 - ay**2 - k**2 + 2*ay*k
    g = 1 + c**2
    h = 2 * (ay*c - ax - c*k)
    i = ar**2 - ax**2 - ay**2 - k**2 + 2*ay*k

    # getting x via quadratic equation
    x1 = (-h + (h**2 + 4*g*i)**0.5)/(2*g)
    x2 = (-h - (h**2 + 4*g*i)**0.5)/(2*g)

    # getting y
    y1 = -x1 * c + k
    y2 = -x2 * c + k

    vect1 = Vect(x1, y1)
    vect2 = Vect(x2, y2)

    r = (vect1+vect2)/2
    # keep in mind this returns a direction, magnitude is wrong which u gonna need to sort out outside of here
    return r




def getNewVelocity(a, b,):
    # magA = getEspeed(a, b, e)
    # magB = getEspeed(b, a, e)
    # a.velocity.mag = magA
    # b.velocity.mag = magB

    # based on the formula found on this https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects
    if b.mass is None:  # its hitting the immovable
        massCoe = 2
    elif a.mass is None:  # it is an immovable object
        massCoe = 0
    else:
        massCoe = 2 * b.mass/(a.mass + b.mass)

    speedCoe = ((a.velocity - b.velocity) * (a.centerOfMass - b.centerOfMass)) / ((a.centerOfMass - b.centerOfMass).mag ** 2)
    # print(a.velocity - ((a.centerOfMass - b.centerOfMass) * (massCoe * speedCoe)))

    return a.velocity - ((a.centerOfMass - b.centerOfMass) * (massCoe * speedCoe))

def getEspeed(a,b, e):
    # using https://en.wikipedia.org/wiki/Inelastic_collision#Formula
    # special cases cause why not
    Va = 0
    if e == 1:  #fully elastic
        Va = a.velocity.mag
    elif e == 0:  #fully inelastic
        Va = (a.mass * a.velocity.mag + b.mass * b.velocity.mag) / (a.mass + b.mass)
    else:
        Va = (e * b.mass(b.velocity.mag - a.velocity.mag) + a.mass * a.velocity.mag + b.mass * b.velocity.mag) / (a.mass + b.mass)
    return Va



def getAcceleration(object):
    if object.mass is None:
        a = Vect(0, 0)
    else:
        a = Vect(object.netForce.x/object.mass, object.netForce.y/object.mass)
    # print(a)
    return a


def getAngularAcceleration(object):
    alpha = object.netTorque / object.inertia
    return alpha


def getAngle(a, b):
    vector1 = getVector(a, b)
    vector2 = a.velocity

    # print(a.number, vector1, vectMod1, b.number, vector2, vectMod2)
    return __getAngle(vector1, vector2)


def __getAngle(vector1, vector2):
    vectMod1 = getVectorMagnitude(vector1)
    vectMod2 = getVectorMagnitude(vector2)
    return math.acos((vector1["x"] * vector2["x"] + vector1["y"] * vector2["y"]) / (vectMod1 * vectMod2)) # was asin before


def getVector(a, b):
    return b.centerOfMass - a.centerOfMass



