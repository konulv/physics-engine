import math
def checkCollisions(data):
    numData = len(data.getAllParticles())
    for i in range(numData):
        a = data.getParticle(i)
        # print("A", a.number, a.position)
        for j in range(i+1, numData):
            b = data.getParticle(j)
            # print("B", b.number, b.position)
            # print(engine.getVector(a, b))
            # print(engine.getVectorMagnitude(engine.getVector(a, b)), "vs", a.radius + b.radius)
            distance = (a.centerOfMass - b.centerOfMass).mag
            if a.radius + b.radius >= distance and check_trajectory(a, b):  # idea is there, but too many edge cases to work
                data.addCollision(a, b)
                print("adding collision")

    # data.collision_train = get_collide_train(data)


def get_collide_train(data):
    collide_trains = []
    for i in data.getAllColliding():
        colliding = [i]
        colliding += data.getAllCollisionsOf(i)
        j = 1
        while j < len(colliding):
            for k in data.getAllCollisionsOf(colliding[j]):
                if k not in colliding:
                    colliding.append(k)
            j += 1

        collide_trains.append(colliding)

    key = 0
    while key < len(collide_trains):
        val = collide_trains[key]
        if len(val) == 1:
            del collide_trains[key]
            continue
        seta = set(val)
        key2 = key+1
        while key2 < len(collide_trains):
            val2 = collide_trains[key2]
            setb = set(val2)
            if len(val2) == 1:
                del collide_trains[key2]
            if seta == setb:
                del collide_trains[key2]
            key2 +=1
        key +=1

    return collide_trains


def check_trajectory(a,b):
    av = a.velocity
    bv = b.velocity
    AB = b.centerOfMass - a.centerOfMass
    BA = a.centerOfMass - b.centerOfMass
    # print(av, AB)
    # print(av ** AB)
    angle1 = math.acos(av ** AB)
    angle2 = math.acos(bv ** BA)

    # this works for now cause i haven't encountered "edge" cases
    # it should break if B is colliding with A as check for that is not made
    # print(a.velocity, b.velocity, angle)
    # pi/2 = 90
    # pi - 1 = 122
    if angle1 < math.pi/2 or angle2 < math.pi/2:  # travel trajectory is in line to actually collide, i.e the objects are moving towards each other
        return True
    else:  # physically touching but objects are moving away from each other
        # i.e. collision maths been done and this is compensating for possibility of objects being inside other objects
        return False


def recheckCollision(a, b):
    distance = (a.centerOfMass - b.centerOfMass).mag
    if a.radius + b.radius >= distance:  # and check_trajectory(a, b):  # idea is there, but too many edge cases to work
        data.addCollision(a, b)
        print("adding collision")

