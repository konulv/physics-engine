
collisionData = {}
collision_train = []
states = {}



def addCollision(a, b):
    __addCollision(a)
    __addCollision(b)
    collisionData[a].append(b)
    collisionData[b].append(a)


def __addCollision(a):
    if a not in collisionData:
        collisionData[a] = []
    else:
        pass


def removeCollision(a, b):
    collisionData[a].remove(b)
    collisionData[b].remove(a)


def getAllCollisionsOf(a):
    return collisionData[a]


def getAllColliding():
    return list(collisionData.keys())


def getParticle(a):
    return states[a]


def addParticle(a):
    if a.count not in states:
        states[a.number] = a


def getAllParticles():
    return states.values()

def printState():
    for i in getAllParticles():
        print(f"{i.number}: pos: {i.position}, speed: {i.velocity}, Force: {i.force}, drag: {i.drag}, net: {i.netForce}")
