import matplotlib.pyplot as plt

plt.axes(xlim=(-20, 20), ylim=(-20, 20))
circle1=plt.Circle((0, 0), 4)

plt.gcf().gca().add_artist(circle1)


plt.show()
# from time import sleep
# #
# #
# def create_circle(x, y, r):
#     circle = plt.Circle((x, y), radius=r)
#     plt.gca().add_patch(circle)
#     # plt.axis('scaled')
#     return circle
# #
# #
# # def show_shape(patch):
#     # ax = plt.gca()
#     # ax.add_patch(patch)
#     # plt.gca().add_patch(patch)
#     # plt.axis('scaled')
# #
# #
# # if __name__ == '__main__':
# #     c = create_circle(0, 0, 4)
# #     c2 = create_circle(3, 3, 2)
# #     show_shape(c)
# #     show_shape(c2)
# #     plt.scatter([x for x in range(10)], [x for x in range(10)])
# #     plt.show()
#
#
# import numpy as np
# from matplotlib import pyplot as plt
# from matplotlib import animation
# from time import sleep
#
# # First set up the figure, the axis, and the plot element we want to animate
# fig = plt.figure()
# fig.clear()
# ax = plt.axes(xlim=(-50, 50), ylim=(-50, 50))
# circlelst = []
# circlelst.append(create_circle(0, 0, 10))
# circlelst.append(create_circle(20, 0, 10))
# circlelst.append(create_circle(10, 17.32, 10))
# # print(circle.center[1])
# # print(np.linspace(0, 2, 1000))
#
#
# # line, = ax.plot([], [], lw=2)
#
# # initialization function: plot the background of each frame
# def init():
#
#     return circlelst
#
# # animation function.  This is called sequentially
# def animate(i):
#     print(i)
#     for j in circlelst:
#         x = j.center[0] + np.linspace(0, 2, 1000)[0]
#         y = j.center[1] + np.sin(2 * np.pi * (x - 0.01 * i))
#         j.set_center((x, y))
#     return circlelst
#
#
# # call the animator.  blit=True means only re-draw the parts that have changed.
# # create_circle(1,0,1)
# # anim = animation.FuncAnimation(fig, animate, init_func=init,
# #                                frames=200, interval=25, blit=True)
#
#
# # save the animation as an mp4.  This requires ffmpeg or mencoder to be
# # installed.  The extra_args ensure that the x264 codec is used, so that
# # the video can be embedded in html5.  You may need to adjust this for
# # your system: for more information, see
# # http://matplotlib.sourceforge.net/api/animation_api.html
# # anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
#
# plt.show()

# import data
# from models.circle import Circle
# import engine
# # a = Circle()
# data.addCollision("a", "b")
# data.addCollision("c", "a")
# data.addParticle("a")
# data.addParticle("b")
# data.addParticle("c")
# print(data.getAllParticles())
# print(data.getAllCollisionsOf("a"))
# print(data.getAllCollisionsOf("b"))
# print(data.getAllCollisionsOf("c"))
# print(engine.getVectorMagnitude({"x": -2.5, "y": 0.5}))