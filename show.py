import json
import matplotlib.pyplot as plt
from models.vector2D import Vector as Vect
from math import sin, cos

with open("simulation.json", "r") as file:
    simulation = json.load(file)

fig, ax = plt.subplots()
for index, tick in enumerate(simulation):
    for circle in tick:
        ax.set_xlim((-15, 15))
        ax.set_ylim((-15, 15))
        ax.add_artist(plt.Circle((circle[1], circle[2]), circle[3]))
        p = Vect(circle[3] * cos(circle[4]), circle[3] * sin(circle[4]))
        p += Vect(circle[1], circle[2])
        ax.plot([circle[1], p.x], [circle[2], p.y], color="black")

    fig.savefig(f"images/{index}.png")
    ax.cla()

# frames = []
# for i in range(FPS * TIME):
#     frames.append(Image.open(f"images/{i}.png"))
#
# frames[0].save("simulation.gif", format="GIF", append_images=frames[1:], save_all=True, duration=TICK / 1000, loop=0)
# print("done")