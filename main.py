import matplotlib.pyplot as plt
from models.circle import Circle
import json
from models.vector2D import Vector as Vect
import data
import engine
import collision
from PIL import Image
from math import sin, cos


###
# Quad trees (?) for resolving collisions
# https://thepythongischallenge.wordpress.com/2014/05/23/introducing-a-pure-python-quadtree-spatial-index-for-gis-use/
# KD tree
# BVH tree
###

def main():
    E = 1  # coefficient of restitution (dictates whatever collision is elastic or not, value between 0-1
    G = 0
    TIME = 5
    FPS = 25
    TICK = 1/FPS
    AIR_DENSITY = 1
    OUTSIDE_VARIABLES = {"g": G, "tick": TICK, "air density": AIR_DENSITY, "e": E}
    x_range = [-5, 15]
    y_range = [-5, 15]

    ## SHOW OFF

    r = 1

    for x in range(0, 10, 2):
        for y in range(0, 10, 2):
            data.addParticle(Circle(r, x, y))

    data.addParticle(Circle(r, -4, 5, mass=1)) # fix overflow thats due to increase in mass AND different size radius
    data.getParticle(25).updateVelocity(Vect(30, 0), 0)
    data.getParticle(25).applyForce(Vect(20, 0))


    # load the input into data
    # for now its just manual loading
    # 3 circle set up
    # data.addParticle(Circle(2, -2, 0))
    # data.addParticle(Circle(2, 4, 1))
    # data.addParticle(Circle(2, 0, 4))
    # data.getParticle(0).updateVelocity(Vect(5, 0), 0)
    # data.getParticle(1).updateVelocity(Vect(-5, 0), 0)
    # data.getParticle(2).updateVelocity(Vect(0, -5), 0)

    # # newton cradle set up
    # data.addParticle(Circle(2, -5, 0))
    # data.addParticle(Circle(2, 0, 0))
    # data.addParticle(Circle(2, 4, 0))
    # data.addParticle(Circle(2, 8, 0))

    # data.addParticle(Circle(2, 8, 0))
    # data.addParticle(Circle(2, 4, 0))
    # data.addParticle(Circle(2, 0, 0))
    # data.addParticle(Circle(2, -5, 0))
    #
    # data.getParticle(3).updateVelocity(Vect(5, 0), 0)

    # 2 collisions at the same time
    # data.addParticle(Circle(2, -5, 0))
    # data.addParticle(Circle(2, 0, 2))
    # data.addParticle(Circle(2, 0, -2))
    # data.getParticle(0).updateVelocity(Vect(5, 0), 0)

    # 2 circle set up
    # data.addParticle(Circle(2, -2, 0, mass=1))
    # data.addParticle(Circle(2, 4, 0))
    # data.getParticle(0).updateVelocity(Vect(5, 0), 0)
    # data.getParticle(1).updateVelocity(Vect(0, 0), 0)

    # # "ground" simulation
    # data.addParticle(Circle(2, 0, 8))
    # data.addParticle(Circle(2, 0, 0, mass=9999999999999999))
    # data.getParticle(0).applyForce(Vect(0, -9.8))


    # opening file for writing
    file = open("simulation.json", "w")
    file.write("[[")  # for the whole thing

    for index, elem in enumerate(data.getAllParticles()):
        # print(elem)
        file.write(f"[{elem.number}, {elem.position.x}, {elem.position.y}, {elem.radius}, {elem.rotation}]")

        if index != len(data.getAllParticles()) - 1:
            file.write(",")


    file.write("],\n")

    for i in range(FPS * TIME):  # total frames to be created
        file.write("[")  # start of a tick
        # send data to detect collisions
        # collision updates data
        print(i)
        collision.checkCollisions(data)

        # printing current state
        # data.printState()



        # send it all to engine now
        engine.doPhysics(data, OUTSIDE_VARIABLES)

        # save the tick to a file
        for index, elem in enumerate(data.getAllParticles()):
            # print(elem)
            file.write(f"[{elem.number}, {elem.position.x}, {elem.position.y}, {elem.radius}, {elem.rotation}]")

            if index != len(data.getAllParticles()) - 1:
                file.write(",")

        file.write("]\n")  # finish of a tick
        if i != FPS*TIME - 1:
            file.write(",")  # there is more coming
    file.write("]")  # finish of simulation
    file.close()

    with open("simulation.json", "r") as file:
        simulation = json.load(file)

    fig, ax = plt.subplots()
    for index, tick in enumerate(simulation):
        for circle in tick:
            ax.set_xlim(x_range)
            ax.set_ylim(y_range)
            ax.add_artist(plt.Circle((circle[1], circle[2]), circle[3]))
            p = Vect(circle[3]*cos(circle[4]), circle[3]*sin(circle[4]))
            p += Vect(circle[1], circle[2])
            ax.plot([circle[1], p.x], [circle[2], p.y], color="black")

        fig.savefig(f"images/{index}.png")
        ax.cla()

    frames = []
    for i in range(FPS*TIME):
        frames.append(Image.open(f"images/{i}.png"))

    frames[0].save("simulation.gif", format="GIF", append_images=frames[1:], save_all=True, duration=TICK/1000, loop=0)
    print("done")


if __name__ == "__main__":
    main()
